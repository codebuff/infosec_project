/***********************************************
Created April 11,2015

Based on the work done by Terry Rogers in his honors thesis at Carson Newman University [http://www.cn.edu/]
His thesis pdf can be accessed at following link
>> http://www.cn.edu/libraries/tiny_mce/tiny_mce/plugins/filemanager/files/Honors2011/Rogers_Terry.pdf

He was unreachable to seek permission to use his code, however under section 1.4 titles "Goal" of his thesis, he writes following line
"For this project, I have designed an open-source firewall for the Windows platform"
Using this line as basis we are using the existing code as base.

Current form of code is modified to make it compatible with newer platform, lot of bug fixes (more than 52)
Also current form of code is much more streamllined as it not scatterred rather all the code is combined into one project
Directry scanning for executable files is also implemented from the scratch.
************************************************/

/***********************************************
Packet Filter Class
This file declares the functions we use in the
Packet Filter class, including port scanning defense
************************************************/
/********Includes********/
#include "stdafx.h"
#include <Winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <strsafe.h>
#include <fwpmu.h>
#include <list>
/********Definitions********/
//Error Codes
//Packet filter is already running
#define PF_ALREADY_RUNNING 11
//Packet filter is now running
#define PF_NOW_RUNNING 1
//Packet filter is already stopped
#define PF_ALREADY_STOPPED 10
//Packet filter is now stopped
#define PF_NOW_STOPPED 0
//Could not stop or start packet filter
#define PF_FAILED -1
//Port Scanning defense is already running
#define PS_ALREADY_RUNNING 22
//Port Scanning defense is now running
#define PS_NOW_RUNNING 2
//Port Scanning defense is already stopped
#define PS_ALREADY_STOPPED 20
//Port Scanning defense is now stopped
#define PS_NOW_STOPPED -20
//Could not stop or start port scanning defense
#define PS_FAILED -2
//Protocols
//Internet Control Message Protocol
#define ICMP 1
//Internet Group Management Protocol
#define IGMP 2
//Bluetooth Radio Frequency Communications Protocol
#define BLUET 3
//Transmission Control Protocol
#define TCP 6
//User Datagram Protocol
#define UDP 17
//Internet Control Message Protocol Version 6
#define ICMP_6 58
//Reliable Multicast protocol
#define RM 113
//Sublayer Names
#define PF_SUBLAYER_NAMEW L"PFDinsFirewall"
#define PS_SUBLAYER_NAMEW L"PSDinsFirewall"
//Default Subnet Mask
#define SUBNET_MASK 0xffffffff
/********Structures********/
//Structure to store IP and ID information for the filter
typedef struct _pFILTERINFO {
	BYTE ByteAddr[4]; //IP address separated by byte for debugging
	ULONG HexAddr; //IP address in Hexadecimal format
	UINT64 OutFilterID; //Outbound filter ID
	UINT64 InFilterID; //Inbound filter ID
	UINT16 Port; //Port indicated with IP address
	UINT8 Protocol; //Protocol indicated
} pFILTERINFO, *PpFILTERINFO;
//List to hold the Filter Infos
typedef std::list<pFILTERINFO> pFILTERINFOLIST;
/********Class Declaration********/
class PacketFilter {
	/********Private Declarations********/
	//The functions and variables should only
	//be used internally by the class
	private:
	//Handle for the Filtering Engine (packet filtering)
	HANDLE pfEngineHandle;
	//Handle for the Filtering engine (port scanning)
	HANDLE psEngineHandle;
	//GUID to identify the sublayer for the packet filter
	GUID pfsubGUID;
	//GUID to identify the sublayer for port scanning prevention
	GUID pssubGUID;
	//List containing filters
	pFILTERINFOLIST pFilterList;
	//Array for the port scanning filter ID keys
	UINT64 psFilterID[4];
	/*PORT SCANNING DEFENSE FUNCTIONS*/
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter Interface is created
	***************************************************/
	DWORD psCreateInterface();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter Interface is deleted
	***************************************************/
	DWORD psDeleteInterface();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter Interface is bound
	***************************************************/
	DWORD psBindInterface();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter Interface is unbound
	***************************************************/
	DWORD psUnbindInterface();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter is added to our interface
	***************************************************/
	DWORD psAddFilter();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter is removed from our interface
	***************************************************/
	DWORD psDelFilter();
	/*PACKET FILTER FUNCTIONS*/
	/***************************************************
	Inputs: cIPAddr: Character pointer formatted IP address
	nStrLen: Length of the IP address string
	bIPAddr: Byte array for the IP address
	hIPAddr: Unsigned long that contains the IP in hexadecimal format
	Returns: True (IP formatted successfully) or
	False (Invalid IP or failure to format)
	Post: The FilterInfo contains the IP address in
	byte array and hexadecimal format
	***************************************************/
	bool FormatIPAddr(char* cIPAddr,
		UINT nStrLen,
		BYTE* bIPAddr,
		ULONG& hIPAddr);
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter Interface is created
	***************************************************/
	DWORD pfCreateInterface();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter Interface is deleted
	***************************************************/
	DWORD pfDeleteInterface();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter Interface is bound
	***************************************************/
	DWORD pfBindInterface();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter Interface is unbound
	***************************************************/
	DWORD pfUnbindInterface();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter is added to our interface
	***************************************************/
	DWORD pfAddFilter();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter is removed from our interface
	***************************************************/
	DWORD pfDelFilter();
	public:
	/********Public Declarations********/
	//The functions and variablescan be accessed from
	//outside of the class
	//Boolean to indicate if packet filter is
	//currently running (true) or not (false)
	bool pfrunning;
	//Boolean to indicate if port scanning defense
	//is currently running (true) or not (false)
	bool psrunning;
	/***************************************************
	Inputs: None
	Returns: None
	Post: Our class is constructed, memory allocated
	***************************************************/
	PacketFilter();
	/***************************************************
	Inputs: None
	Returns: None
	Post: Our class has been destructed
	***************************************************/
	~PacketFilter();
	/***************************************************
	Inputs: Character pointer formatted IP address,
	Unsigned integer (8 bits) protocol
	Unsigned integer (16 bits) port
	Returns: None
	Post: The IP is added to our list of IPs to block
	***************************************************/
	void AddIPToBlockList(char* cIPAddr, UINT8 proto, UINT16 prt);
	/***************************************************
	Inputs: None
	Returns: Integer indicating running state (see Error Codes)
	Post: The Packet Filter is running and active
	***************************************************/
	int pfStartFirewall();
	/***************************************************
	Inputs: None
	Returns: Integer indicating running state (see Error Codes)
	Post: The Packet Filter is stopped and inactive
	***************************************************/
	int pfStopFirewall();
	/***************************************************
	Inputs: None
	Returns: Integer indicating running state (see Error Codes)
	Post: The Port Scanning Defense is running and active
	***************************************************/
	int psStartFirewall();
	/***************************************************
	Inputs: None
	Returns: Integer indicating running state (see Error Codes)
	Post: The Port Scanning Defense is stopped and inactive
	***************************************************/
	int psStopFirewall();
};