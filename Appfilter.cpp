/***********************************************
Created April 11,2015

Based on the work done by Terry Rogers in his honors thesis at Carson Newman University [http://www.cn.edu/]
His thesis pdf can be accessed at following link
>> http://www.cn.edu/libraries/tiny_mce/tiny_mce/plugins/filemanager/files/Honors2011/Rogers_Terry.pdf

He was unreachable to seek permission to use his code, however under section 1.4 titles "Goal" of his thesis, he writes following line
"For this project, I have designed an open-source firewall for the Windows platform"
Using this line as basis we are using the existing code as base.

Current form of code is modified to make it compatible with newer platform, lot of bug fixes (more than 52)
Also current form of code is much more streamllined as it not scatterred rather all the code is combined into one project
Directry scanning for executable files is also implemented from the scratch.
************************************************/

/***********************************************
Application Filter Class
This file implements the functions we use in
the Application Filter class
************************************************/
/********Includes********/
#include "stdafx.h"
#include "Appfilter.h"
/********Functions********/
/***************************************************
Inputs: None
Returns: None
Post: Our class is constructed, memory allocated
***************************************************/
AppFilter::AppFilter() {
	appEngineHandle = NULL; //Initialize the handle
	//Allocate memory for GUID, fill with 0s
	ZeroMemory(&appsubGUID, sizeof(GUID));
}
/***************************************************
Inputs: None
Returns: None
Post: Our class has been destructed
***************************************************/
AppFilter::~AppFilter() {
	appStopFirewall();
}
/***************************************************
Inputs: None
Returns: Integer indicating running state (see Error Codes)
Post: The application filter is running and active
***************************************************/
int AppFilter::appStartFirewall() {
	//If the Application filter is already running...
	if ( apprunning ) {
		return APP_ALREADY_RUNNING; //Indicate filter is currently running
	}
	else { //Filter is not running...
		//Create the application filter interface
		if ( appCreateInterface() == ERROR_SUCCESS ) {
			//Bind the application filter interface
			if ( appBindInterface() == ERROR_SUCCESS ) {
				//Add the filters
				if ( appAddFilter() == ERROR_SUCCESS ) {
					//Application filter is now running
					apprunning = true;
					//Message indicating filter is running
					return APP_NOW_RUNNING;
				}
			}
		}
		//Starting application filter failed at some point
		return APP_FAILED;
	}
}
	
   /***************************************************
	Inputs: None
	Returns: Integer indicating running state (see Error Codes)
	Post: The application filter is stopped and inactive
	**************************************************/
	int AppFilter::appStopFirewall(){
		//If the application filter is not running...
		if ( !apprunning ) {
			//Indicate filter is already stopped
			return APP_ALREADY_STOPPED;
		}
		else {//Filter is running
			if ( appDelFilter() == ERROR_SUCCESS ) {
				aFilterList.clear(); //Clear our filter list
				if ( appUnbindInterface() == ERROR_SUCCESS ) {
					if ( appDeleteInterface() == ERROR_SUCCESS ) {
						//Firewall is no longer running
						apprunning = false;
						//Message indicated application filter is no longer running
						return APP_NOW_STOPPED;
					}
				}
			}
		}
		//Stopping application filter failed at some point
		return APP_FAILED;
	}
	/***************************************************
	Inputs: String formatted Application path
	Returns: None
	Post: The app is added to our list of applications to block
	***************************************************/
	void AppFilter::AddAppToBlockList(char* cPath,unsigned int length) {
		//Structure to store application info
		aFILTERINFO Appfilter = { 0 };
		//Initialize wide character format variable for filepath
		wchar_t *wPath = (wchar_t *)malloc(sizeof(wchar_t) * length);
		//Convert the filepath into wide character format
		int result = mbstowcs(wPath, cPath, length); // #todo use mbstowcs_s here
		if ( result != -1 ) { //If conversion was successful
			//Store the newly formatted path
			Appfilter.appPath = wPath;
			//Add the structure to the list
			aFilterList.push_back(Appfilter);
		}
	}
		/***************************************************
		Inputs: None
		Returns: Error code indicating any errors
		Post: The Filter Interface is created
		***************************************************/
		DWORD AppFilter::appCreateInterface() {
			//ErrorCode for WFP functions, default is bad command
			DWORD ErrorCode = ERROR_BAD_COMMAND;
			//Create packet filter interface by
			//opening a session with the filtering engine
			ErrorCode = FwpmEngineOpen0(//Must be NULL
				NULL,
				//Specifies authentication service to use
				RPC_C_AUTHN_WINNT,
				//Authorize credentials for filter engine (optional)
				NULL,
				//Session specific parameters (optional)
				NULL,
				//Engine handle set
				&appEngineHandle);
			//Error code indicating any errors or success
			return ErrorCode;
		}
		/***************************************************
		Inputs: None
		Returns: Error code indicating any errors
		Post: The Filter Interface is deleted
		***************************************************/
		DWORD AppFilter::appDeleteInterface() {
			//ErrorCode for WFP functions, default is bad command
			DWORD ErrorCode = ERROR_BAD_COMMAND;
			//If our engine handle is still set properly (not NULL)
			if ( appEngineHandle != NULL ) {
				//Close the interface
				ErrorCode = FwpmEngineClose0(appEngineHandle);
				//Reset the Engine Handle (to NULL)
				appEngineHandle = NULL;
			}
			//Error code indicating any errors or success
			return ErrorCode;
		}
		/***************************************************
		Inputs: None
		Returns: Error code indicating any errors
		Post: The Filter Interface is bound
		***************************************************/
		DWORD AppFilter::appBindInterface() {
			//ErrorCode for WFP functions, default is bad command
			DWORD ErrorCode = ERROR_BAD_COMMAND;
			//Remote Procedure Call status
			RPC_STATUS rpcStatus = { 0 };
			//Sublayer structure
			FWPM_SUBLAYER0 SubLayer = { 0 };
			//Create a GUID for the SubLayer for easy access
			rpcStatus = UuidCreate(&SubLayer.subLayerKey);
			if ( rpcStatus == NO_ERROR ) {
				/*Save the GUID for future use. Copies memory directly from
				SubLayer to appsubGUID from beginning of subLayerKey to the size of subLayerKey*/
				CopyMemory(&appsubGUID,
					&SubLayer.subLayerKey,
					sizeof(SubLayer.subLayerKey));
				//Fill in the SubLayer information
				SubLayer.displayData.name = APP_SUBLAYER_NAMEW;
				SubLayer.displayData.description = APP_SUBLAYER_NAMEW;
				SubLayer.flags = 0;
				SubLayer.weight = 0x100; //Importance of the sublayer
				//Add the SubLayer to the interface
				ErrorCode = FwpmSubLayerAdd0(appEngineHandle,
					&SubLayer,
					NULL);
			}
			//Error code indicating any errors or success
			return ErrorCode;
		}
		/***************************************************
		Inputs: None
		Returns: Error code indicating any errors
		Post: The Filter Interface is unbound
		***************************************************/
		DWORD AppFilter::appUnbindInterface() {
			//Delete the sublayer from the interface
			DWORD ErrorCode = FwpmSubLayerDeleteByKey0(appEngineHandle,
				&appsubGUID);
			//Fill the GUID memory space with 0s
			ZeroMemory(&appsubGUID, sizeof(GUID));
			//Error code indicating any errors or success
			return ErrorCode;
		}
		/***************************************************
		Inputs: None
		Returns: Error code indicating any errors
		Post: The Filter is added to our interface
		***************************************************/
		DWORD AppFilter::appAddFilter() {
			//ErrorCode for WFP functions, default is bad command
			DWORD ErrorCode = ERROR_BAD_COMMAND;
			//Make sure we have filters to add
			if ( aFilterList.size() > 0 ) {
				//Create an iterator to keep track of what filter we are on
				aFILTERINFOLIST::iterator numFilter;
				//For each filter in our list
				for ( numFilter = aFilterList.begin();
					numFilter != aFilterList.end();
					numFilter++ ) {
					//Constant number of conditions required
					const UINT8 numCond = 1;
					//Filter structure for filter info
					FWPM_FILTER0 Filter;
					//Fill filter structure with 0s
					ZeroMemory(&Filter, sizeof(FWPM_FILTER));
					//Filter condition structure for
					//trigger condition info (1 for each condition)
					FWPM_FILTER_CONDITION0 Conditions[numCond];
					//Fill filter condition structures with 0s
					ZeroMemory(&Conditions,
						sizeof(FWPM_FILTER_CONDITION)*numCond);
					//Byte blob structure for storing app data
					FWP_BYTE_BLOB *appBlob = NULL;
					//Boolean to check if the transaction is in progress
					bool txninProgress = FALSE;
					//Begin the transaction
					ErrorCode = FwpmTransactionBegin0(appEngineHandle, 0);
					txninProgress = TRUE;
					//Get data from the filepath given, store in appBlob
					DWORD result = FwpmGetAppIdFromFileName0(numFilter->appPath,
						&appBlob);
					/*Fill condition in with needed information for Application Data*/
					//Look in the APP_ID field of the packet
					Conditions[0].fieldKey = FWPM_CONDITION_ALE_APP_ID;
					//Trigger when APP_ID is equal to our filter
					Conditions[0].matchType = FWP_MATCH_EQUAL;
					//We are comparing Byte Blobs that contain our app info
					Conditions[0].conditionValue.type = FWP_BYTE_BLOB_TYPE;
					//The appBlob info, including Application ID
					Conditions[0].conditionValue.byteBlob = appBlob;
					/*Fill filter in with needed information*/
					//GUID for easy access and deletion
					Filter.subLayerKey = appsubGUID;
					//Name of Filter for displaying only
					Filter.displayData.name = APP_SUBLAYER_NAMEW;
					//Inspect when resources (ports) are assigned (bound)
					Filter.layerKey = FWPM_LAYER_ALE_RESOURCE_ASSIGNMENT_V4;
					//Block applications that meet our
					//condition from binding to a port in V4
					Filter.action.type = FWP_ACTION_BLOCK;
					//Let the engine auto assign the weight of the filter
					Filter.weight.type = FWP_EMPTY;
					//Conditions to be fulfilled for action
					//to be taken (previous condition struct)
					Filter.filterCondition = Conditions;
					//Number of conditions on our filter
					Filter.numFilterConditions = numCond;
					//Add our filter to the engine
					ErrorCode = FwpmFilterAdd0(appEngineHandle,
						&Filter,
						NULL,
						&(numFilter->FilterID4));
					//If not a success, begin cleanup
					/*NOTE: goto in this instance, and all other instances,
					**is used purely to prevent the code from being clogged
					**up with multiple nested if blocks or the creation of
					**otherwise unneeded variables. It is merely the easiest
					**and most efficient way to preform the transaction*/
					if ( ErrorCode != ERROR_SUCCESS ) goto CLEANUP;
					//Change the filter layer to the V6 layer
					Filter.layerKey = FWPM_LAYER_ALE_RESOURCE_ASSIGNMENT_V6;
					//Add our filter to the engine
					ErrorCode = FwpmFilterAdd0(appEngineHandle,
						&Filter,
						NULL,
						&(numFilter->FilterID6));
					//If not a success, begin cleanup
					/*NOTE: goto in this instance, and all other instances,
					**is used purely to prevent the code from being clogged
					**up with multiple nested if blocks or the creation of
					**otherwise unneeded variables. It is merely the easiest
					**and most efficient way to preform the transaction*/
					if ( ErrorCode != ERROR_SUCCESS ) goto CLEANUP;
					/*If everything succeeded, then we add the filters atomically
					**At this point we are actually adding the filters. Previous
					**code is now executed*/
					ErrorCode = FwpmTransactionCommit0(appEngineHandle);
					txninProgress = FALSE;
					//Cleanup operation if any filter additions failed
				CLEANUP:
					//if transaction is in progress and we had an error
					//(from a goto) then abort the transaction (do nothing)
					if ( (txninProgress) && (ErrorCode != ERROR_SUCCESS) ) {
						//Abort the transaction currently in progress
						FwpmTransactionAbort0(appEngineHandle);
					}
					//Error code indicating any errors or success
					return ErrorCode;
				}
			}
		}
				/***************************************************
				Inputs: None
				Returns: Error code indicating any errors
				Post: The Filter is removed from our interface
				***************************************************/
		DWORD AppFilter::appDelFilter() {
			//ErrorCode for WFP functions, default is bad command
			DWORD ErrorCode = ERROR_BAD_COMMAND;
			//Make sure our list is not empty
			if ( aFilterList.size() > 0 ) {
				//Create an iterator to go through our list
				aFILTERINFOLIST::iterator numFilter;
				//Delete all the filters we added
				for ( numFilter = aFilterList.begin();
					numFilter != aFilterList.end();
					numFilter++ ) {
					//Delete the filter via the FilterID4 (GUID for IPv4)
					ErrorCode = FwpmFilterDeleteById0
						(appEngineHandle,
						numFilter->FilterID4);
					//Delete the filter via the FilterID6 (GUID for IPv6)
					ErrorCode = FwpmFilterDeleteById0
						(appEngineHandle,
						numFilter->FilterID6);
					//Reset the ID fields
					numFilter->FilterID4 = 0;
					numFilter->FilterID6 = 0;
					//If an error is encoutered, stop trying
					//to delete filters and report it
					if ( ErrorCode != ERROR_SUCCESS ) break;
				}
				//Error code indicating any errors or success
				return ErrorCode;
			}
		}