/***********************************************
Created April 11,2015

Based on the work done by Terry Rogers in his honors thesis at Carson Newman University [http://www.cn.edu/]
His thesis pdf can be accessed at following link
>> http://www.cn.edu/libraries/tiny_mce/tiny_mce/plugins/filemanager/files/Honors2011/Rogers_Terry.pdf

He was unreachable to seek permission to use his code, however under section 1.4 titles "Goal" of his thesis, he writes following line
"For this project, I have designed an open-source firewall for the Windows platform"
Using this line as basis we are using the existing code as base.

Current form of code is modified to make it compatible with newer platform, lot of bug fixes (more than 52)
Also current form of code is much more streamllined as it not scatterred rather all the code is combined into one project
Directry scanning for executable files is also implemented from the scratch.
************************************************/

/***********************************************
Packet Filter Class
This file implements the functions we use in the
Packet Filter class, including port scanning defense
************************************************/
/********Includes********/
#include "stdafx.h"
#include "Packetfilter.h"

/********Functions********/
/***************************************************
Inputs: None
Returns: None
Post: Our class is constructed, memory allocated
***************************************************/
PacketFilter::PacketFilter() {
	//Initialize the handles
	pfEngineHandle = NULL;
	psEngineHandle = NULL;
	//Allocate memory for GUID, fill with 0s
	ZeroMemory(&pfsubGUID, sizeof(GUID));
	ZeroMemory(&pssubGUID, sizeof(GUID));
	

}
/***************************************************
Inputs: None
Returns: None
Post: Our class has been destructed
***************************************************/
PacketFilter::~PacketFilter() {
	//Stop the packet filter before closing the app
	pfStopFirewall();
	//Stop the port scanning defense before closing the app
	psStopFirewall();
}
/***************************************************
Inputs: Character pointer formatted IP address,
Unsigned integer (8 bits) protocol
Unsigned integer (16 bits) port
Returns: None
Post: The IP is added to our list of IPs to block
***************************************************/
void PacketFilter::AddIPToBlockList(char* cIPAddr,UINT8 proto,UINT16 prt) {
	//Check to make sure cIPAddr has some value (check validity later)
	if ( cIPAddr != NULL ) {
		pFILTERINFO IPFilter = { 0 }; //Initialize the FilterInfo struct
		/*Fill in the FilterInfo structure with our data*/
		//Fill in the port number
		IPFilter.Port = prt;
		//Fill in the protocol value
		IPFilter.Protocol = proto;
		//Get byte array format and hex format IP address from
		//string format. Only add to the list if IP is valid
		//and successfully formatted
		if ( FormatIPAddr(cIPAddr,strlen(cIPAddr),IPFilter.ByteAddr,IPFilter.HexAddr) )
		{
			//Add the filter to the list
			pFilterList.push_back(IPFilter);
		}
	}
}

/***************************************************
Inputs: None
Returns: Integer indicating running state (see Error Codes)
Post: The packet filter is running and active
***************************************************/
int PacketFilter::pfStartFirewall() {
	//Check if firewall is already running
	if ( pfrunning ) {
		//Indicate packet filter is already running
		return PF_ALREADY_RUNNING;
	}
	else {
		//Create the packet filter interface
		if ( pfCreateInterface() == ERROR_SUCCESS ) {
			//Bind the packet filter interface
			if ( pfBindInterface() == ERROR_SUCCESS ) {
				//Add the filters
				if ( pfAddFilter() == ERROR_SUCCESS ) {
					//Packet filter is now running
					pfrunning = true;
					//Message indicating packet filter is running
					return PF_NOW_RUNNING;
				}
			}
		}
	}
	//Failed at some point in starting
	//the packet filter
	return PF_FAILED;
}



/***************************************************
Inputs: None
Returns: Integer indicating running state (see Error Codes)
Post: The packet filter is stopped and inactive
***************************************************/
int PacketFilter::pfStopFirewall() {
	//If the packet filter is not running
	if ( !pfrunning ) {
		//Indicate the packet filter is already stopped
		return PF_ALREADY_STOPPED;
	}
	else //If the packet filter is running
	{
		//Delete the filters
		pfDelFilter();
		//Clear out the filter list
		pFilterList.clear();
		//Unbind from the packet filter interface
		if ( pfUnbindInterface() == ERROR_SUCCESS ) {
			//Delete the packet filter interface
			if ( pfDeleteInterface() == ERROR_SUCCESS ) {
				//Packet filter is no longer running
				pfrunning = false;
				//Return indicating packet filter is now stopped
				return PF_NOW_STOPPED;
			}
		}
	}
	//Failed at some point in stopping the packet filter
	return PF_FAILED;
}
/***************************************************
Inputs: None
Returns: Integer indicating running state (see Error Codes)
Post: The port scanning defense is running and active
***************************************************/


int PacketFilter::psStartFirewall() {
	//Check if port scanning defense is already running
	if ( psrunning ) {
		//Indicates port scanning defense is currently running
		return PS_ALREADY_RUNNING;
	}
	else {
		//Create the port scanning defense interface
		if ( psCreateInterface() == ERROR_SUCCESS ) {
			//Bind the port scanning defense interface
			if ( psBindInterface() == ERROR_SUCCESS ) {
				//Add the filters
				if ( psAddFilter() == ERROR_SUCCESS ) {
					//port scanning defense is now running
					psrunning = true;
					//Message indicating port scanning defense is running
					return PS_NOW_RUNNING;
				}
			}
		}
	}
	//Failed at some point in starting
	//the port scanning defense
	return PS_FAILED;
}

/***************************************************
Inputs: None
Returns: Integer indicating running state (see Error Codes)
Post: The port scanning defense is stopped and inactive
***************************************************/
int PacketFilter::psStopFirewall() {
	//If the port scanning defense is not running
	if ( !psrunning ) {
		//Message indicating port scanning defense already stopped
		return PS_ALREADY_STOPPED;
	}
	else {
		

			//Delete the filters
			psDelFilter();
		//Unbind from the port scanning defense interface
		if ( psUnbindInterface() == ERROR_SUCCESS ) {
			//Delete the port scanning defense interface
			if ( psDeleteInterface() == ERROR_SUCCESS ) {
				//Port scanning defense is no longer running
				psrunning = false;
				//Message indicating port scanning defense is now stopped
				return PS_NOW_STOPPED;
			}
		}
	}
	//Failed at some point in stopping
	//the port scanning defense
	return PS_FAILED;
}

/***************************************************
Inputs: None
Returns: Error code indicating any errors
Post: The port scanning Interface is created
***************************************************/
DWORD PacketFilter::psCreateInterface() {
	//ErrorCode for WFP functions, default is bad command
	DWORD ErrorCode = ERROR_BAD_COMMAND;
	//Create port scanning defense interface by
	//opening a session with the filtering engine
	ErrorCode = FwpmEngineOpen0(//Must be NULL
		NULL,
		//Specifies authentication service to use
		RPC_C_AUTHN_WINNT,
		//Authorize credentials for filter engine (optional)
		NULL,
		//Session specific parameters (optional)
		

		NULL,
		//Engine handle set
		&psEngineHandle);
	//Error code indicating any errors or success
	return ErrorCode;
}
/***************************************************
Inputs: None
Returns: Error code indicating any errors
Post: The port scanning Interface is deleted
***************************************************/
DWORD PacketFilter::psDeleteInterface() {
	//ErrorCode for WFP functions, default is bad command
	DWORD ErrorCode = ERROR_BAD_COMMAND;
	//Check to see our Engine Handle is still correct
	if ( psEngineHandle != NULL ) {
		//Close the interface
		ErrorCode = FwpmEngineClose0(psEngineHandle);
		psEngineHandle = NULL; //Reset the Engine Handle
	}
	//Error code indicating any errors or success
	return ErrorCode;
}

/***************************************************
Inputs: None
Returns: Error code indicating any errors
Post: The port scanning defense Interface is bound
***************************************************/
DWORD PacketFilter::psBindInterface() {
	//ErrorCode for WFP functions, default is bad command
	DWORD ErrorCode = ERROR_BAD_COMMAND;
	//Remote Procedure Call status
	RPC_STATUS rpcStatus = { 0 };
	//Sublayer data structure
	FWPM_SUBLAYER0 SubLayer = { 0 };
	

		//Create a GUID for the SubLayer
		//for easy access and deletion
		rpcStatus = UuidCreate(&SubLayer.subLayerKey);
	if ( rpcStatus == NO_ERROR ) {
		/*Save the GUID for future use. Copies memory
		**directly from SubLayer to pssubGUID from
		**beginning of subLayerKey to the size of subLayerKey*/
		CopyMemory(&pssubGUID,
			&SubLayer.subLayerKey,
			sizeof(SubLayer.subLayerKey));
		//Fill in the SubLayer information
		SubLayer.displayData.name = PS_SUBLAYER_NAMEW;
		SubLayer.displayData.description = PS_SUBLAYER_NAMEW;
		SubLayer.flags = 0;
		SubLayer.weight = 0x100; //Importance of the sublayer
		//Add the SubLayer to the interface
		ErrorCode = FwpmSubLayerAdd0(psEngineHandle,
			&SubLayer,
			NULL);
	}
	//Error code indicating any errors or success
	return ErrorCode;
}
/***************************************************
Inputs: None
Returns: Error code indicating any errors
Post: The port scanning defense Interface is unbound
***************************************************/
DWORD PacketFilter::psUnbindInterface() {
	//Delete the sublayer from the interface
	DWORD ErrorCode = FwpmSubLayerDeleteByKey0(psEngineHandle,
		&pssubGUID);
	//Fill the GUID memory space with 0s
	ZeroMemory(&pssubGUID, sizeof(GUID));

	

		//Error code indicating any errors or success
		return ErrorCode;
}
/***************************************************
Inputs: None
Returns: Error code indicating any errors
Post: The port scanning defense filters are added
to our interface
***************************************************/
DWORD PacketFilter::psAddFilter() {
	//ErrorCode for WFP functions, default is bad command
	DWORD ErrorCode = ERROR_BAD_COMMAND;
	//Constant number of conditions required
	const UINT8 numCond = 1;
	//Filter structure for filter info
	FWPM_FILTER0 Filter;
	//Fill filter structure with 0s
	ZeroMemory(&Filter, sizeof(FWPM_FILTER));
	//Filter condition structure for trigger condition info
	FWPM_FILTER_CONDITION0 Conditions;
	//Fill filter condition structure with 0s
	ZeroMemory(&Conditions, sizeof(FWPM_FILTER_CONDITION));
	//Boolean to check if the transaction is in progress
	bool txninProgress = FALSE;
	/*Fill filter in with needed information*/
	//Name of Filter for displaying only
	Filter.displayData.name = PS_SUBLAYER_NAMEW;
	//GUID for easy access and deletion
	Filter.subLayerKey = pssubGUID;
	//Number of conditions on our filter
	Filter.numFilterConditions = numCond;
	//Conditions to be fulfilled for action
	//to be taken (previous condition struct)
	Filter.filterCondition = &Conditions;
	/*Since we�re adding multiple conditions at once,
	**use a transaction to clean up any partial additions
	**in case of errors*/
	

		ErrorCode = FwpmTransactionBegin0(psEngineHandle, 0);
	txninProgress = TRUE;
	/*Block outbound ICMP Destination Unreachable
	to prevent UDP port scanning*/
	Conditions.fieldKey = FWPM_CONDITION_ICMP_TYPE;
	Conditions.matchType = FWP_MATCH_EQUAL;
	Conditions.conditionValue.type = FWP_UINT16;
	Conditions.conditionValue.uint16 = 3;
	Filter.action.type = FWP_ACTION_BLOCK;
	//Add the filter for V4
	Filter.layerKey = FWPM_LAYER_OUTBOUND_ICMP_ERROR_V4;
	ErrorCode = FwpmFilterAdd0(psEngineHandle,
		&Filter,
		NULL,
		&psFilterID[0]);
	//If not a success, begin cleanup
	/*NOTE: goto in this instance, and all other instances,
	**is used purely to prevent the code from being clogged
	**up with multiple nested if blocks or the creation of
	**otherwise unneeded variables. It is merely the easiest
	**and most efficient way to preform the transaction*/
	if ( ErrorCode != ERROR_SUCCESS ) goto CLEANUP;
	//Add the filter for V6
	Filter.layerKey = FWPM_LAYER_OUTBOUND_ICMP_ERROR_V6;
	ErrorCode = FwpmFilterAdd0(psEngineHandle,
		&Filter,
		NULL,
		&psFilterID[1]);
	//If not a success, begin cleanup
	/*NOTE: goto in this instance, and all other instances,
	**is used purely to prevent the code from being clogged
	**up with multiple nested if blocks or the creation of
	**otherwise unneeded variables. It is merely the easiest
	87

	**and most efficient way to preform the transaction*/
	if ( ErrorCode != ERROR_SUCCESS ) goto CLEANUP;
	/*Block outbound TCP RST to prevent
	**TCP port scanning*/
	//Don�t want to block RST from loopback, so create a
	//condition that matches everything but loopback
	Conditions.fieldKey = FWPM_CONDITION_FLAGS;
	Conditions.matchType = FWP_MATCH_FLAGS_NONE_SET;
	Conditions.conditionValue.type = FWP_UINT32;
	Conditions.conditionValue.uint32 = FWP_CONDITION_FLAG_IS_LOOPBACK;
	Filter.action.type = FWP_ACTION_CALLOUT_TERMINATING;
	//Add the filter for IPv4 on the discard layer
	Filter.layerKey = FWPM_LAYER_INBOUND_TRANSPORT_V4_DISCARD;
	Filter.action.calloutKey =
		FWPM_CALLOUT_WFP_TRANSPORT_LAYER_V4_SILENT_DROP;
	ErrorCode = FwpmFilterAdd0(psEngineHandle,
		&Filter,
		NULL,
		&psFilterID[2]);
	//If not a success, begin cleanup
	/*NOTE: goto in this instance, and all other instances,
	**is used purely to prevent the code from being clogged
	**up with multiple nested if blocks or the creation of
	**otherwise unneeded variables. It is merely the easiest
	**and most efficient way to preform the transaction*/
	if ( ErrorCode != ERROR_SUCCESS ) goto CLEANUP;
	//Add the filter for IPv6 on the discard layer
	Filter.layerKey = FWPM_LAYER_INBOUND_TRANSPORT_V6_DISCARD;
	Filter.action.calloutKey =
		FWPM_CALLOUT_WFP_TRANSPORT_LAYER_V6_SILENT_DROP;
	ErrorCode = FwpmFilterAdd0(psEngineHandle,
		&Filter,
		NULL,
		&psFilterID[3]);
	

		//If not a success, begin cleanup
		/*NOTE: goto in this instance, and all other instances,
		**is used purely to prevent the code from being clogged
		**up with multiple nested if blocks or the creation of
		**otherwise unneeded variables. It is merely the easiest
		**and most efficient way to preform the transaction*/
		if ( ErrorCode != ERROR_SUCCESS ) goto CLEANUP;
	//If everything succeeded, then we add the filters atomically
	//At this point we are actually adding the filters
	ErrorCode = FwpmTransactionCommit0(psEngineHandle);
	txninProgress = FALSE;
	//Cleanup operation if any filter additions failed
CLEANUP:
	//if transaction is in progress and we had an error
	//(from a goto) then abort the transaction (do nothing)
	if ( (txninProgress) && (ErrorCode != ERROR_SUCCESS) ) {
		//Abort the transaction currently in progress
		FwpmTransactionAbort0(psEngineHandle);
	}
	//Error code indicating any errors or success
	return ErrorCode;
}

/***************************************************
Inputs: None
Returns: Error code indicating any errors
Post: The port scanning defense filters are removed
from our interface
***************************************************/
DWORD PacketFilter::psDelFilter() {
	//ErrorCode for WFP functions, default is bad command
	DWORD ErrorCode = ERROR_BAD_COMMAND;
	//For all 4 of our filters
	for ( int i = 0; i < 4; i++ ) {
		

			//Delete the filter via the ID (GUID)
			ErrorCode = FwpmFilterDeleteById0(psEngineHandle,
			psFilterID[i]);
		//Reset the ID field
		psFilterID[i] = 0;
	}
	//Error code indicating any errors or success
	return ErrorCode;
}

/***************************************************
Inputs: cIPAddr: String formatted IP address
nStrLen: Length of the IP address string
bIPAddr: Byte array for the IP address
hIPAddr: Unsigned long that contains the
IP in hexadecimal format
Returns: True (IP formatted successfully)
or False (Invalid IP)
Post: The FilterInfo contains the IP address
in byte array and hexadecimal format
***************************************************/
bool PacketFilter::FormatIPAddr(char* cIPAddr,
	UINT nStrLen,
	BYTE* bIPAddr,
	ULONG& hIPAddr) {
	//The current character we are on
	UINT i = 0;
	//The current octet we are on
	UINT j = 0;
	//Stores the octet value
	UINT valOct = 0;
	//Temporary storage for the current character
	char temp;
	// Build byte array format from string format.
	for ( ; (i < nStrLen) && (j < 4); i++ ) {
		

			//If we are still on the same octet
			if ( cIPAddr[i] != '.' ) {
				//Copy the character to temp
				temp = cIPAddr[i];
				//Update the current octet value
				valOct = (valOct * 10) + (temp - '0');
			}
			else //If we are starting a new octet
			{
				//Invalid octet value
				if ( (valOct < 0) || (valOct > 255) ) return false;
				//Store the octet value in the byte array
				bIPAddr[j] = valOct;
				valOct = 0; //Reset the octet value
				j++;
				//We are on a new octet
			}
	}
	//IP has less then 4 octets (invalid IP address)
	if ( j != 3 ) return false;
	//Store the last octect (we do not encounter
	//another �.� at the end of the string)
	bIPAddr[j] = valOct;
	//Format into hexadecimal from the byte array
	for ( int k = 0; k < 4; k++ ) {
		//Use bitshifts to properly format the IP
		hIPAddr = (hIPAddr << 8) + bIPAddr[k];
	}
	//IP address successfully formatted and valid
	return true;
}

/***************************************************
Inputs: None


Returns: Error code indicating any errors
 Post: The packet filter Interface is created
 ***************************************************/
DWORD PacketFilter::pfCreateInterface() {
	//ErrorCode for WFP functions, default is bad command
	DWORD ErrorCode = ERROR_BAD_COMMAND;
	//Create packet filter interface by opening
	//a session with the filtering engine
	ErrorCode = FwpmEngineOpen0(//Must be NULL
		NULL,
		//Specifies authentication service to use
		RPC_C_AUTHN_WINNT,
		//Authorize credentials for filter engine (optional)
		NULL,
		//Session specific parameters (optional)
		NULL,
		//Engine handle set
		&pfEngineHandle);
	//Error code indicating any errors or success
	return ErrorCode;
}
/***************************************************
Inputs: None
Returns: Error code indicating any errors
Post: The packet filter Interface is deleted
***************************************************/
DWORD PacketFilter::pfDeleteInterface() {
	//ErrorCode for WFP functions, default is bad command
	DWORD ErrorCode = ERROR_BAD_COMMAND;
	//Check to see our Engine Handle is still correct
	if ( pfEngineHandle != NULL ) {
		//Close the interface
		ErrorCode = FwpmEngineClose0(pfEngineHandle);
		pfEngineHandle = NULL; //Reset the Engine Handle
	}

	

		//Error code indicating any errors or success
		return ErrorCode;
}

/***************************************************
Inputs: None
Returns: Error code indicating any errors
Post: The packet filter Interface is bound
***************************************************/
DWORD PacketFilter::pfBindInterface() {
	//ErrorCode for WFP functions, default is bad command
	DWORD ErrorCode = ERROR_BAD_COMMAND;
	//Remote Procedure Call status
	RPC_STATUS rpcStatus = { 0 };
	//Sublayer structure
	FWPM_SUBLAYER0 SubLayer = { 0 };
	//Create a GUID for the SubLayer
	//for easy access and deletion
	rpcStatus = UuidCreate(&SubLayer.subLayerKey);
	if ( rpcStatus == NO_ERROR ) {
		/*Save the GUID for future use. Copies memory
		**directly from SubLayer to pssubGUID from
		**beginning of subLayerKey to the size of subLayerKey*/
		CopyMemory(&pfsubGUID,
			&SubLayer.subLayerKey,
			sizeof(SubLayer.subLayerKey));
		//Fill in the SubLayer information
		SubLayer.displayData.name = PF_SUBLAYER_NAMEW;
		SubLayer.displayData.description = PF_SUBLAYER_NAMEW;
		SubLayer.flags = 0;
		SubLayer.weight = 0x100; //Importance of the sublayer
		//Add the SubLayer to the interface
		ErrorCode = FwpmSubLayerAdd0(pfEngineHandle,
			&SubLayer,
			NULL);
	}
	

		//Error code indicating any errors or success
		return ErrorCode;
}
/***************************************************
Inputs: None
Returns: Error code indicating any errors
Post: The packet filter Interface is unbound
***************************************************/
DWORD PacketFilter::pfUnbindInterface() {
	//Delete the sublayer from the interface
	DWORD ErrorCode = FwpmSubLayerDeleteByKey0(pfEngineHandle,
		&pfsubGUID);
	//Fill the GUID memory space with 0s
	ZeroMemory(&pfsubGUID, sizeof(GUID));
	//Error code indicating any errors or success
	return ErrorCode;
}
/***************************************************
Inputs: None
Returns: Error code indicating any errors
Post: The Filter is added to our interface for both
outbound and inbound traffic
***************************************************/
DWORD PacketFilter::pfAddFilter() {
	//ErrorCode for WFP functions, default is bad command
	DWORD ErrorCode = ERROR_BAD_COMMAND;
	//Make sure we have filters to add
	if ( pFilterList.size() > 0 ) {
		//Create an iterator to keep track
		//of what filter we are on
		pFILTERINFOLIST::iterator numFilter;
		//For each filter in our list
		for ( numFilter = pFilterList.begin();
			numFilter != pFilterList.end();
			

			numFilter++ ) {
			//Constant number of conditions required
			const UINT8 numCond = 3;
			//Filter structure for filter info
			FWPM_FILTER0 Filter;
			//Fill filter structure with 0s
			ZeroMemory(&Filter, sizeof(FWPM_FILTER));
			//Filter condition structure for trigger
			//condition info (1 for each condition)
			FWPM_FILTER_CONDITION0 Conditions[numCond];
			//Fill filter condition structures with 0s
			ZeroMemory(&Conditions,
				sizeof(FWPM_FILTER_CONDITION) * numCond);
			//IP address and subnet mask data structure (v4)
			FWP_V4_ADDR_AND_MASK AddrMask = { 0 };
			//Boolean to check if the transaction is in progress
			bool txninProgress = FALSE;
			//Begin the transaction
			ErrorCode = FwpmTransactionBegin0(pfEngineHandle, 0);
			txninProgress = TRUE;
			/*IP address info*/
			//IP Address in Hexadecimal format
			AddrMask.addr = numFilter->HexAddr;
			//Subnet mask (default global subnet mask)
			AddrMask.mask = SUBNET_MASK;
			/*Fill condition in with needed information for IP address*/
			//Check the remote address (where packet is coming from)
			Conditions[0].fieldKey =
				FWPM_CONDITION_IP_REMOTE_ADDRESS;
			//IP addresses should equal to trigger condition
			Conditions[0].matchType = FWP_MATCH_EQUAL;
			//We are comparing IPv4 addresses and masks
			Conditions[0].conditionValue.type = FWP_V4_ADDR_MASK;
			//IP address and subnet mask we are checking for
			Conditions[0].conditionValue.v4AddrMask = &AddrMask;

			

				/*Fill condition in with needed information for Protocol*/
				 //Check the protocol
				 Conditions[1].fieldKey = FWPM_CONDITION_IP_PROTOCOL;
			//Protocol should be the same to trigger condition
			Conditions[1].matchType = FWP_MATCH_EQUAL;
			//Protocol is an unsigned Integer of 8 bits
			Conditions[1].conditionValue.type = FWP_UINT8;
			//Protocol we want to block is in numFilter
			Conditions[1].conditionValue.uint32 =
				numFilter->Protocol;
			/*Fill condition in with needed information for Port*/
			//Check the incoming port number
			Conditions[2].fieldKey = FWPM_CONDITION_IP_REMOTE_PORT;
			//0 indicates we want to block all ports
			if ( (numFilter->Port) == 0 ) {
				//All ports greater than or
				//equal to 0 (in other words, all)
				Conditions[2].matchType = FWP_MATCH_GREATER_OR_EQUAL;
			}
			//We want to block a specific port
			else {
				//Match the specified port
				Conditions[2].matchType = FWP_MATCH_EQUAL;
			}
			//Ports are unsigned Integers of 16 bits
			Conditions[2].conditionValue.type = FWP_UINT16;
			//Port we want to block is in numFilter
			Conditions[2].conditionValue.uint16 = numFilter->Port;
			/*Fill filter in with needed information*/
			//GUID for easy access
			Filter.subLayerKey = pfsubGUID;
			//Name of Filter for displaying only
			Filter.displayData.name = PF_SUBLAYER_NAMEW;
			//Inspect incoming IPv4 packets
			Filter.layerKey = FWPM_LAYER_INBOUND_TRANSPORT_V4;
			//Block packets that meet our conditions
			Filter.action.type = FWP_ACTION_BLOCK;
			
				//Let the engine auto assign the weight
				Filter.weight.type = FWP_EMPTY;
			//Conditions to be fulfilled for action to be taken
			Filter.filterCondition = Conditions;
			//Number of conditions on our filter
			Filter.numFilterConditions = numCond;
			//Add the filter for inbound traffic
			ErrorCode = FwpmFilterAdd0(pfEngineHandle,
				&Filter,
				NULL,
				&(numFilter->InFilterID));
			//If not a success, begin cleanup
			/*NOTE: goto in this instance, and all other instances,
			**is used purely to prevent the code from being clogged
			**up with multiple nested if blocks or the creation of
			**otherwise unneeded variables. It is merely the easiest
			**and most efficient way to preform the transaction*/
			if ( ErrorCode != ERROR_SUCCESS ) goto CLEANUP;
			//Change the layer to the outbound layer
			Filter.layerKey = FWPM_LAYER_OUTBOUND_TRANSPORT_V4;
			//Add the filter for outbound
			ErrorCode = FwpmFilterAdd0(pfEngineHandle,
				&Filter,
				NULL,
				&(numFilter->OutFilterID));
			//If not a success, begin cleanup
			/*NOTE: goto in this instance, and all other instances,
			**is used purely to prevent the code from being clogged
			**up with multiple nested if blocks or the creation of
			**otherwise unneeded variables. It is merely the easiest
			**and most efficient way to preform the transaction*/
			if ( ErrorCode != ERROR_SUCCESS ) goto CLEANUP;
			//Commit the transaction
			ErrorCode = FwpmTransactionCommit0(pfEngineHandle);
			

				txninProgress = FALSE;
			//Cleanup operation if any filter additions failed
		CLEANUP:
			//if transaction is in progress and we had an error
			//(from a goto) then abort the transaction (do nothing)
			if ( (txninProgress) && (ErrorCode != ERROR_SUCCESS) ) {
				FwpmTransactionAbort0(pfEngineHandle);
			}
		}
	}
	//Error code indicating any errors or success
	return ErrorCode;
}

/***************************************************
Inputs: None
Returns: Error code indicating any errors
Post: The Filter is removed from our interface
***************************************************/
DWORD PacketFilter::pfDelFilter() {
	//ErrorCode for WFP functions, default is bad command
	DWORD ErrorCode = ERROR_BAD_COMMAND;
	//Make sure our list is not empty
	if ( pFilterList.size() > 0 ) {
		//Create an iterator to go through our list
		pFILTERINFOLIST::iterator numFilter;
		//For each filter in the list
		for ( numFilter = pFilterList.begin();
			numFilter != pFilterList.end();
			numFilter++ ) {
			//Delete the inbound traffic filter
			ErrorCode = FwpmFilterDeleteById0(pfEngineHandle,
				numFilter->InFilterID);
			//Delete the outbound traffic filter
			if ( ErrorCode == ERROR_SUCCESS ) {
				ErrorCode = FwpmFilterDeleteById0(pfEngineHandle,
					

					numFilter->OutFilterID);
				numFilter->OutFilterID = 0;
			}
			//Reset the ID
			numFilter->InFilterID = 0;
			//If an error is encoutered, stop
			//trying to delete filters and report it
			if ( ErrorCode != ERROR_SUCCESS ) break;
		}
	}
	//Error code indicating any errors or success
	return ErrorCode;
}