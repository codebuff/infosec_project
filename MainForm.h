/***********************************************
Created April 12,2015

Based on the work done by Terry Rogers in his honors thesis at Carson Newman University [http://www.cn.edu/]
His thesis pdf can be accessed at following link
>> http://www.cn.edu/libraries/tiny_mce/tiny_mce/plugins/filemanager/files/Honors2011/Rogers_Terry.pdf

He was unreachable to seek permission to use his code, however under section 1.4 titles "Goal" of his thesis, he writes following line
"For this project, I have designed an open-source firewall for the Windows platform"
Using this line as basis we are using the existing code as base.

Current form of code is modified to make it compatible with newer platform, lot of bug fixes (more than 52)
Also current form of code is much more streamllined as it not scatterred rather all the code is combined into one project
Directry scanning for executable files is also implemented from the scratch.
************************************************/

/***********************************************
MainForm Class
This file handles all GUI functions and data
************************************************/
/********Includes********/
#include "Packetfilter.h"
#include "Appfilter.h"
#include <iostream> // for debugging purposes
/********Structures********/
//Structure to store inputted IP addresses
//until we start the firewall
typedef struct _PFBlocked {
	char *IP;
	UINT16 Port;
	UINT8 Protocol;
} PFBlocked, *PPFBlocked;
//List of inputted IP addresses
typedef std::list<PFBlocked> PFBlockedList;
//Structure to store selected applications to block
typedef struct _APPBlocked {
	char *appBlock;
	unsigned int length;
} APPBlocked, *PAPPBlocked;
//List of selected applications
typedef std::list<APPBlocked> APPBlockedList;
/********Global Variables********/
//List of selected applications to block
APPBlockedList appList;
//List of inputted IP addresses to block
PFBlockedList pfList;
//Packet Filter
PacketFilter pf;
//Application Filter
AppFilter af;


#pragma once

namespace infosec_project_win32 {

	using namespace System;
	using namespace System::IO;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MainForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form {
		public:
		MainForm(void) {
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainForm() {
			if ( components ) {
				delete components;
			}
		}

		private: System::Windows::Forms::Button^ start;
		protected:
		private: System::Windows::Forms::Button^ stop;
		private: System::Windows::Forms::TextBox^ txtPfStat;
		private: System::Windows::Forms::Label^ lblStat;
		private: System::Windows::Forms::Button^ btnAddBlk;
		private: System::Windows::Forms::TextBox^ txtNewIP;
		private: System::Windows::Forms::Button^ btnAppGather;
		private: System::Windows::Forms::Label^ lblApp;
		private: System::Windows::Forms::CheckedListBox^ chklistApps;
		private: System::Windows::Forms::Label^ label1;
		private: System::Windows::Forms::TextBox^ txtPort;
		private: System::Windows::Forms::ComboBox^ comboPrtcl;
		private: System::Windows::Forms::Label^ label2;
		private: System::Windows::Forms::Label^ label3;
		private: System::Windows::Forms::Label^ label4;
		private: System::Windows::Forms::TextBox^ txtAppStat;
		private: System::Windows::Forms::Button^ btnCurFile;
		private: System::Windows::Forms::Label^ label5;
		private: System::Windows::Forms::TextBox^ txtPsStat;
		private: System::Windows::Forms::CheckBox^ chkPS;


		private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void) {
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MainForm::typeid));
			this->start = (gcnew System::Windows::Forms::Button());
			this->stop = (gcnew System::Windows::Forms::Button());
			this->txtPfStat = (gcnew System::Windows::Forms::TextBox());
			this->lblStat = (gcnew System::Windows::Forms::Label());
			this->btnAddBlk = (gcnew System::Windows::Forms::Button());
			this->txtNewIP = (gcnew System::Windows::Forms::TextBox());
			this->btnAppGather = (gcnew System::Windows::Forms::Button());
			this->lblApp = (gcnew System::Windows::Forms::Label());
			this->chklistApps = (gcnew System::Windows::Forms::CheckedListBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->txtPort = (gcnew System::Windows::Forms::TextBox());
			this->comboPrtcl = (gcnew System::Windows::Forms::ComboBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->txtAppStat = (gcnew System::Windows::Forms::TextBox());
			this->btnCurFile = (gcnew System::Windows::Forms::Button());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->txtPsStat = (gcnew System::Windows::Forms::TextBox());
			this->chkPS = (gcnew System::Windows::Forms::CheckBox());
			this->SuspendLayout();
			// 
			// start
			// 
			this->start->Location = System::Drawing::Point(12, 306);
			this->start->Name = L"start";
			this->start->Size = System::Drawing::Size(75, 23);
			this->start->TabIndex = 7;
			this->start->Text = L"Start Firewall";
			this->start->UseVisualStyleBackColor = true;
			this->start->Click += gcnew System::EventHandler(this, &MainForm::start_Click);
			// 
			// stop
			// 
			this->stop->Location = System::Drawing::Point(192, 306);
			this->stop->Name = L"stop";
			this->stop->Size = System::Drawing::Size(75, 23);
			this->stop->TabIndex = 8;
			this->stop->Text = L"Stop Firewall";
			this->stop->UseVisualStyleBackColor = true;
			this->stop->Click += gcnew System::EventHandler(this, &MainForm::stop_Click);
			// 
			// txtPfStat
			// 
			this->txtPfStat->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->txtPfStat->Location = System::Drawing::Point(96, 9);
			this->txtPfStat->Name = L"txtPfStat";
			this->txtPfStat->ReadOnly = true;
			this->txtPfStat->Size = System::Drawing::Size(100, 20);
			this->txtPfStat->TabIndex = 2;
			this->txtPfStat->TabStop = false;
			this->txtPfStat->Text = L"Now Stopped";
			// 
			// lblStat
			// 
			this->lblStat->AutoSize = true;
			this->lblStat->Location = System::Drawing::Point(9, 9);
			this->lblStat->Name = L"lblStat";
			this->lblStat->Size = System::Drawing::Size(81, 13);
			this->lblStat->TabIndex = 3;
			this->lblStat->Text = L"PF Filter Status:";
			// 
			// btnAddBlk
			// 
			this->btnAddBlk->Location = System::Drawing::Point(192, 156);
			this->btnAddBlk->Name = L"btnAddBlk";
			this->btnAddBlk->Size = System::Drawing::Size(60, 46);
			this->btnAddBlk->TabIndex = 3;
			this->btnAddBlk->Text = L"Add To Blocked";
			this->btnAddBlk->UseVisualStyleBackColor = true;
			this->btnAddBlk->Click += gcnew System::EventHandler(this, &MainForm::btnAddBlk_Click);
			// 
			// txtNewIP
			// 
			this->txtNewIP->Location = System::Drawing::Point(76, 105);
			this->txtNewIP->Name = L"txtNewIP";
			this->txtNewIP->Size = System::Drawing::Size(97, 20);
			this->txtNewIP->TabIndex = 0;
			// 
			// btnAppGather
			// 
			this->btnAppGather->Location = System::Drawing::Point(382, 27);
			this->btnAppGather->Name = L"btnAppGather";
			this->btnAppGather->Size = System::Drawing::Size(101, 40);
			this->btnAppGather->TabIndex = 4;
			this->btnAppGather->Text = L"Begin Filepath Gathering";
			this->btnAppGather->UseVisualStyleBackColor = true;
			this->btnAppGather->Click += gcnew System::EventHandler(this, &MainForm::btnAppGather_Click);
			// 
			// lblApp
			// 
			this->lblApp->AutoSize = true;
			this->lblApp->Location = System::Drawing::Point(379, 9);
			this->lblApp->Name = L"lblApp";
			this->lblApp->Size = System::Drawing::Size(118, 13);
			this->lblApp->TabIndex = 7;
			this->lblApp->Text = L"Not Gathering Filepaths";
			// 
			// chklistApps
			// 
			this->chklistApps->CheckOnClick = true;
			this->chklistApps->FormattingEnabled = true;
			this->chklistApps->HorizontalScrollbar = true;
			this->chklistApps->Location = System::Drawing::Point(298, 135);
			this->chklistApps->Name = L"chklistApps";
			this->chklistApps->Size = System::Drawing::Size(246, 184);
			this->chklistApps->Sorted = true;
			this->chklistApps->TabIndex = 6;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 108);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(58, 13);
			this->label1->TabIndex = 9;
			this->label1->Text = L"IP Address";
			// 
			// txtPort
			// 
			this->txtPort->Location = System::Drawing::Point(77, 140);
			this->txtPort->Name = L"txtPort";
			this->txtPort->Size = System::Drawing::Size(95, 20);
			this->txtPort->TabIndex = 1;
			// 
			// comboPrtcl
			// 
			this->comboPrtcl->AllowDrop = true;
			this->comboPrtcl->FormattingEnabled = true;
			this->comboPrtcl->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"TCP", L"UDP", L"ICMP", L"ICMP_6", L"IGMP", L"BLUET",
					L"RM"
			});
			this->comboPrtcl->Location = System::Drawing::Point(76, 181);
			this->comboPrtcl->Name = L"comboPrtcl";
			this->comboPrtcl->Size = System::Drawing::Size(102, 21);
			this->comboPrtcl->TabIndex = 2;
			this->comboPrtcl->Text = L"Select a Protocol";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 184);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(51, 13);
			this->label2->TabIndex = 12;
			this->label2->Text = L"Protocols";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(12, 143);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(26, 13);
			this->label3->TabIndex = 13;
			this->label3->Text = L"Port";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(9, 41);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(84, 13);
			this->label4->TabIndex = 14;
			this->label4->Text = L"AppFilter Status:";
			// 
			// txtAppStat
			// 
			this->txtAppStat->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->txtAppStat->Location = System::Drawing::Point(96, 38);
			this->txtAppStat->Name = L"txtAppStat";
			this->txtAppStat->ReadOnly = true;
			this->txtAppStat->Size = System::Drawing::Size(100, 20);
			this->txtAppStat->TabIndex = 15;
			this->txtAppStat->TabStop = false;
			this->txtAppStat->Text = L"Now Stopped";
			// 
			// btnCurFile
			// 
			this->btnCurFile->Location = System::Drawing::Point(384, 81);
			this->btnCurFile->Name = L"btnCurFile";
			this->btnCurFile->Size = System::Drawing::Size(98, 30);
			this->btnCurFile->TabIndex = 5;
			this->btnCurFile->Text = L"Use current file";
			this->btnCurFile->UseVisualStyleBackColor = true;
			this->btnCurFile->Click += gcnew System::EventHandler(this, &MainForm::btnCurFile_Click);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(9, 70);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(87, 13);
			this->label5->TabIndex = 16;
			this->label5->Text = L"PortScan Status:";
			// 
			// txtPsStat
			// 
			this->txtPsStat->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->txtPsStat->Location = System::Drawing::Point(96, 67);
			this->txtPsStat->Name = L"txtPsStat";
			this->txtPsStat->ReadOnly = true;
			this->txtPsStat->Size = System::Drawing::Size(100, 20);
			this->txtPsStat->TabIndex = 17;
			this->txtPsStat->Text = L"Now Stopped";
			// 
			// chkPS
			// 
			this->chkPS->AutoSize = true;
			this->chkPS->Location = System::Drawing::Point(12, 225);
			this->chkPS->Name = L"chkPS";
			this->chkPS->Size = System::Drawing::Size(142, 17);
			this->chkPS->TabIndex = 18;
			this->chkPS->Text = L"Port Scanning Defense\?";
			this->chkPS->UseVisualStyleBackColor = true;
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(569, 384);
			this->Controls->Add(this->chkPS);
			this->Controls->Add(this->txtPsStat);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->btnCurFile);
			this->Controls->Add(this->txtAppStat);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->comboPrtcl);
			this->Controls->Add(this->txtPort);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->chklistApps);
			this->Controls->Add(this->lblApp);
			this->Controls->Add(this->btnAppGather);
			this->Controls->Add(this->txtNewIP);
			this->Controls->Add(this->btnAddBlk);
			this->Controls->Add(this->lblStat);
			this->Controls->Add(this->txtPfStat);
			this->Controls->Add(this->stop);
			this->Controls->Add(this->start);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Name = L"MainForm";
			this->Text = L"Infosec Project";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		/*Start the Packet Filter and Application firewalls,
		if applicable, when "Start" button is clicked*/
		private: System::Void start_Click
			(System::Object^ sender, System::EventArgs^ e) {
			//Build the list of applications based on checked items
			APPBlockBuilder();
			//Add all the IPs to the block list
			PFBlockedList::iterator PfnumFilter;
			for ( PfnumFilter = pfList.begin();
				PfnumFilter != pfList.end();
				PfnumFilter++ ) {
				pf.AddIPToBlockList(PfnumFilter->IP,
					PfnumFilter->Protocol,
					PfnumFilter->Port);
			}
			//Add all apps to the block list
			APPBlockedList::iterator AppnumFilter;
			for ( AppnumFilter = appList.begin();
				AppnumFilter != appList.end();
				AppnumFilter++ ) {
				af.AddAppToBlockList(AppnumFilter->appBlock,
					AppnumFilter->length);
			}
			//Check if port scanning defense is desired
			//If it is checked, start it
			if ( chkPS->Checked ) {
				int psresult = pf.psStartFirewall();
				//Update the display based on any errors
				if ( psresult == PS_ALREADY_RUNNING )
					txtPsStat->Text = "Already running";
				else {
					if ( psresult == PS_NOW_RUNNING )
						txtPsStat->Text = "Now running";
					else
						txtPsStat->Text = "Error!";
				}
			}
			//Else, update the display to indicate it is not running
			else txtPsStat->Text = "Not Running";
			//Check if any IPs have been added to be blocked
			if ( !pfList.empty() ) {
				int pfresult = pf.pfStartFirewall();
				//Update the display based on any errors
				if ( pfresult == PF_ALREADY_RUNNING )
					txtPfStat->Text = "Already running";
				else {
					if ( pfresult == PF_NOW_RUNNING )
						txtPfStat->Text = "Now running";
					else
						txtPfStat->Text = "Error!";
				}
			}
			//Else, update the display to indicate it is not running
			else txtPfStat->Text = "Not Running";
			//Check if any applications were selected
			if ( !appList.empty() ) {
				//Start the Application Firewall
				int appresult = af.appStartFirewall();
				//Update the display based on any errors
				if ( appresult == APP_ALREADY_RUNNING )
					txtAppStat->Text = "Already running";
				else {
					if ( appresult == APP_NOW_RUNNING )
						txtAppStat->Text = "Now running";
					else
						txtAppStat->Text = "Error!";
				}
			}
			//Else, update the display to indicate it is not running
			else txtAppStat->Text = "Not Running";
		}
				 /*Stop the Port Scanning Defense,
				 Packet Filter, and Application Filter
				 when the "Stop" button is clicked*/
		private: System::Void stop_Click
			(System::Object^ sender, System::EventArgs^ e) {
			//Stop the Port Scanning Defense
			int psresult = pf.psStopFirewall();
			//Update the display based on any errors
			if ( psresult == PS_ALREADY_STOPPED )
				txtPsStat->Text = "Already stopped";
			else {
				if ( psresult == PS_NOW_STOPPED )
					txtPsStat->Text = "Now stopped";
				else
					txtPsStat->Text = "Error!";
			}
			//Stop the Packet Filter Firewall
			int pfresult = pf.pfStopFirewall();
			//Clear our list of blocked IPs
			pfList.clear();
			//Update the display based on any errors
			if ( pfresult == PF_ALREADY_STOPPED )
				txtPfStat->Text = "Already Stopped";
			else {
				if ( pfresult == PF_NOW_STOPPED )
					txtPfStat->Text = "Now Stopped";
				else
					txtPfStat->Text = "Error!";
			}
			//Stop the Application Firewall
			int appresult = af.appStopFirewall();
			//Clear our list of blocked apps
			appList.clear();
			//Update the display based on any errors
			if ( appresult == APP_ALREADY_STOPPED )
				txtAppStat->Text = "Already Stopped";
			else {
				if ( appresult == APP_NOW_STOPPED )
					txtAppStat->Text = "Now Stopped";
				else
					txtAppStat->Text = "Error!";
			}
		}
				 /*Add an IP address to the list of IPs to
				 block when the "Add To Blocked" button is clicked*/
		private: System::Void btnAddBlk_Click
			(System::Object^ sender, System::EventArgs^ e) {
			//Make sure the packet filter is not
			//running before adding new IPs
			if ( !pf.psrunning ) {
				//Create a new data structure to store the info
				PFBlocked Blocked = { 0 };
				//Format the IP address to character
				//pointer, add to storage structure
				Blocked.IP = (char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(txtNewIP->Text)).ToPointer();
				//Format the Port number, add to storage structure
				short port;
				Int16::TryParse(txtPort->Text, port);
				Blocked.Port = (UINT16)port;
				//Cases depending on which combo box item is
				//selected for the protocol
				switch ( comboPrtcl->SelectedIndex ) {
					case 0: //TCP
						Blocked.Protocol = TCP; //Protocol is TCP
						pfList.push_back(Blocked); //Add the structure to the list
						txtPort->Text = ""; //Clear the port text box
						txtNewIP->Text = ""; //Clear the IP text box
						txtNewIP->Focus(); //Put cursor back in IP text box
						break;
					case 1: //UDP
						Blocked.Protocol = UDP; //Protocol is UDP
						pfList.push_back(Blocked); //Add the structure to the list
						txtPort->Text = ""; //Clear the port text box
						txtNewIP->Text = ""; //Clear the IP text box
						txtNewIP->Focus(); //Put cursor back in IP text box
						break;
					case 2: //ICMP
						Blocked.Protocol = ICMP; //Protocol is ICMP
						pfList.push_back(Blocked); //Add the structure to the list
						txtPort->Text = ""; //Clear the port text box
						txtNewIP->Text = ""; //Clear the IP text box
						txtNewIP->Focus(); //Put cursor back in IP text box
						break;
					case 3: //ICMP Version 6
						Blocked.Protocol = ICMP_6; //Protocol is ICMP V6
						pfList.push_back(Blocked); //Add the structure to the list
						txtPort->Text = ""; //Clear the port text box
						txtNewIP->Text = ""; //Clear the IP text box
						txtNewIP->Focus(); //Put cursor back in IP text box
						break;
					case 4: //IGMP
						Blocked.Protocol = IGMP; //Protocol is IGMP
						pfList.push_back(Blocked); //Add the structure to the list
						txtPort->Text = ""; //Clear the port text box
						txtNewIP->Text = ""; //Clear the IP text box
						txtNewIP->Focus(); //Put cursor back in IP text box
						break;
					case 5: //Bluetooth
						Blocked.Protocol = BLUET; //Protocol is Bluetooth
						pfList.push_back(Blocked); //Add the structure to the list
						txtPort->Text = ""; //Clear the port text box
						txtNewIP->Text = ""; //Clear the IP text box
						txtNewIP->Focus(); //Put cursor back in IP text box
						break;
					case 6: //RM
						Blocked.Protocol = RM; //Protocol is RM
						pfList.push_back(Blocked); //Add the structure to the list
						txtPort->Text = ""; //Clear the port text box
						txtNewIP->Text = ""; //Clear the IP text box
						txtNewIP->Focus(); //Put cursor back in IP text box
						break;
					default: //Error or no protocol selected
						MessageBox::Show("Please select a protocol!");
				}
			}
			else { //Cannot add IPs while filter is already running
				MessageBox::Show("You must stop the firewall " +
					"before adding new IPs");
				//Have the "Stop" button selected (bring into focus)
				stop->Focus();
			}
		}
				 /*Build a list of all applications selected to be blocked*/
		private: void APPBlockBuilder() {
			//Get a list of all checked items
			IEnumerator^ Enum = chklistApps->CheckedItems->GetEnumerator();
			//Go through each checked item
			while ( Enum->MoveNext() ) {
				//Current checked object we are on
				Object^ Checked = safe_cast<Object^>(Enum->Current);
				//Filepath of the checked object
				String^ path = gcnew String(Checked->ToString());
				//Add a null char to indicate end of string
				path = String::Concat(path, "\0");
				//Create a structure to store the data
				APPBlocked Blocked = { 0 };
				//Get the length of the filepath for later type conversion
				Blocked.length = path->Length;
				//Store the filepath (converted to character pointer)
				//into the structure
				Blocked.appBlock = (char*)(System::Runtime::InteropServices::
					Marshal::StringToHGlobalAnsi(path)).ToPointer();
				//Add the structure to the list of blocked applications
				appList.push_back(Blocked);
			}
		}
				 /*Gather Filepaths of all Applications (all .exe)*/
		private: System::Void btnAppGather_Click
			(System::Object^ sender, System::EventArgs^ e) {
			//Clear the box before we populate it with new filepaths
			chklistApps->Items->Clear();
			//If the filepath file exists, delete it to
			//make room for our new, more current one.
			if ( File::Exists("AppList.txt") ) {
				File::Delete("AppList.txt");
			}
			
			//_tprintf(_T("%s\n"), e);
			//Search recursively for Applications 
			
			AppSearch(Directory::GetCurrentDirectory());
			//AppSearch("C:\\"); lets leave it for later part
			//Update label to indicate scan completed
			lblApp->Text = "Scan complete";
		}
				 /*Recursively search through directories to find all .exe files*/
		private: void AppSearch(String^ sDir) {
			//MessageBox::Show("root dir " + sDir);
			//Error code for getting attributes
			DWORD result;
			//Character pointer formatted filepath
			//to gather folder attributes
			//char* attributes; 
			wchar_t* attributes;
			try {
				// take record of all the files in the root dir
				
					array<String^>^ files = Directory::GetFiles(sDir, "*.exe");
					//Get the number of files found in the directory
					int numFile = files->GetLength(0);
					//MessageBox::Show(numFile.ToString());
					//For each file that has a .exe extension
					for ( int j = 0; j < numFile; j++ ) {
						//Open the text file (created if it does not exist)
						StreamWriter^ writer = File::AppendText("AppList.txt");
						//Append the filepath to the text file on a new line
						writer->WriteLine(files[j]);
						//Add the file to the list on the GUI unselected
						chklistApps->Items->Add(files[j], CheckState::Unchecked);
						//Close the file writer
						writer->Close();
					}
				//Find all the subfolders in the folder that is passed in
				array<String^>^ dirs = Directory::GetDirectories(sDir);
				//Get the total number of subfolders
				int numDir = dirs->GetLength(0);
				//MessageBox::Show("no of dir in root" + numDir.ToString());
				for ( int i = 0; i < numDir; i++ ) { //For each subfolder
					//MessageBox::Show(dirs[i]);
					//Format the folder path into a character pointer
					attributes = (wchar_t*)(System::Runtime::InteropServices::
						Marshal::StringToHGlobalAnsi(dirs[i])).ToPointer();
					//Get the attributes of the folder
					result = GetFileAttributes(attributes);

					if ( (result == FILE_ATTRIBUTE_REPARSE_POINT) || (dirs[i]->Contains("System Volume")) ) {
						i++;
						//MessageBox::Show("junction point or contains SV in " + dirs[i]);
					}
						//Begin search on next folder, if there is one
						AppSearch(dirs[i]);
					}
				}
			//}
			//If an exception (error) occurs
			catch ( System::Exception^ e ) {
				//Display the exception (error)
				MessageBox::Show(e->Message);
			}
		}
				 /*Load up a previously created list of applications if
				 the "Use Current File" button is clicked*/
		private: System::Void btnCurFile_Click
			(System::Object^ sender, System::EventArgs^ e) {
			//Clear the list before we populate it with new entries
			chklistApps->Items->Clear();
			//Open the file for reading
			StreamReader^ reader = gcnew StreamReader("AppList.txt");
			try {
				String^ line; //String for each line of text
				//While there is still a valid line
				while ( line = reader->ReadLine() ) {
					//Add the line to the box
					chklistApps->Items->Add(line, CheckState::Unchecked);
				}
			}
			catch ( System::Exception^ e ) { //If an exception (error) occurs
				MessageBox::Show(e->Message); //Display the exception (error)
			}
			//Close the file
			reader->Close();
			//Update label display
			lblApp->Text = "File loading complete";
		}

	};
}
