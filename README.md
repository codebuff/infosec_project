**Instructions to run the firewall**

1. First download inf_sec_project_portable.zip  
1. Extract the file, and click on the infosec_project.exe 

**Dependency** : .NET 4.5 and visual C++ runtime  

**else**  

**If you don't see any such thing and encounter error then probably some dependencies are missing on your system.In this case** 

1. Download the setup and it will automatically install the dependencies and exe file for you, and will create a desktop shortcut, 
1. Click on the shortcut on desktop and you will the see the GUI of the app.

**<< you will need to run the app with administrative privileges >>**  
**<< AppList.txt needs to be in the same directory as infosec.exe >>**

Thats it, test the app and report back any error.

**Behavior of the Gather file button** >> It gather all the exe files in the directory which infosec exe is running, thus if you want to list all the exe files in C directory then put the infosec.exe in C directory and click on the button.