// kindly contact deepankar tyagi before making any changes here.


#include "stdafx.h"
#include "MainForm.h"

using namespace infosec_project_win32;

[STAThreadAttribute]
int main(array<System::String ^> ^args) {
	/* displaying an arbitary message to give the info
	MessageBox::Show("\t      A firewall still in development.\nIf anything bad happens feel free to shout at Deepankar Tyagi.\n\t      Kindly report back any bugs.");*/

	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	// Create the main window and run it
	Application::Run(gcnew MainForm());
	return 0;
}