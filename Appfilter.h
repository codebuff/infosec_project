/***********************************************
Created April 11,2015

Based on the work done by Terry Rogers in his honors thesis at Carson Newman University [http://www.cn.edu/]
His thesis pdf can be accessed at following link
>> http://www.cn.edu/libraries/tiny_mce/tiny_mce/plugins/filemanager/files/Honors2011/Rogers_Terry.pdf

He was unreachable to seek permission to use his code, however under section 1.4 titles "Goal" of his thesis, he writes following line
"For this project, I have designed an open-source firewall for the Windows platform"
Using this line as basis we are using the existing code as base.

Current form of code is modified to make it compatible with newer platform, lot of bug fixes (more than 52)
Also current form of code is much more streamllined as it not scatterred rather all the code is combined into one project
Directry scanning for executable files is also implemented from the scratch.
************************************************/

/***********************************************
Application Filter Class
This file declares the functions are used in the
Application Filter class
************************************************/
/********Includes********/
#include "stdafx.h"
#include <Winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <strsafe.h>
#include <fwpmu.h>
#include <list>
/********Definitions********/
//Error Codes
#define APP_ALREADY_RUNNING 11 //App Filter is already running
#define APP_NOW_RUNNING 1 //App Filter is now started
#define APP_ALREADY_STOPPED 10 //App filter is already stopped
#define APP_NOW_STOPPED 0 //App filter is now stopped
#define APP_FAILED -1 //Could not start or start the App filter
//Sublayer Name
#define APP_SUBLAYER_NAMEW L"APPDinsFirewall"
/********Structures********/
//Structure to store IP and ID information for the filter
typedef struct _aFILTERINFO {
	UINT64 FilterID4; //Filter ID for IPv4
	UINT64 FilterID6; //Filter ID for IPv6
	DWORD appID; //Application ID
	wchar_t *appPath; //Application filepath
} aFILTERINFO, *PaFILTERINFO;
//List to hold the Filter Infos
typedef std::list<aFILTERINFO> aFILTERINFOLIST;
/********Class Declaration********/
class AppFilter {
	/********Private Declarations********/
	//These functions and variables should only
	//be used internally by the class
	private:
	//Handle for the Filtering Engine
	HANDLE appEngineHandle;
	//GUID to identify the sublayer
	GUID appsubGUID;
	//List containing filters
	aFILTERINFOLIST aFilterList;
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter Interface is created
	***************************************************/
	DWORD appCreateInterface();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter Interface is deleted
	***************************************************/
	DWORD appDeleteInterface();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter Interface is bound
	***************************************************/
	DWORD appBindInterface();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter Interface is unbound
	***************************************************/
	DWORD appUnbindInterface();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter is added to our interface
	***************************************************/
	DWORD appAddFilter();
	/***************************************************
	Inputs: None
	Returns: Error code indicating any errors
	Post: The Filter is removed from our interface
	***************************************************/
	DWORD appDelFilter();
	public:
	/********Public Declarations********/
	//The functions and variables that can
	//be accessed from outside of the class
	//Boolean to indicate if firewall is
	//currently running (true) or not (false)
	bool apprunning;
	/***************************************************
	Inputs: None
	Returns: None
	Post: Our class is constructed, memory allocated
	***************************************************/
	AppFilter();
	/***************************************************
	Inputs: None
	Returns: None
	Post: Our class has been destructed
	***************************************************/
	~AppFilter();
	/***************************************************
	Inputs: None
	Returns: Integer indicating running state (see Error Codes)
	Post: The Application filter is running and active
	***************************************************/
	int appStartFirewall();
	/***************************************************
	Inputs: None
	Returns: Integer indicating running state (see Error Codes)
	Post: The Application filter is stopped and inactive
	***************************************************/
	int appStopFirewall();
	/***************************************************
	Inputs: Character pointer formatted Application path
	Integer with application path length
	Returns: None
	Post: The app is added to our list of applications to block
	***************************************************/
	void AddAppToBlockList(char* sPath, unsigned int length);
};